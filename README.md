# WP Social Feeds

A plugin for storing and caching social feed data. This plugin simply acts as a 'proxy' between social network APIs and your theme's JavaScript code. It does not store individual feed items as WordPress posts; rather, it simply caches the feeds and delivers them to your JavaScript as if they were being sent directly from the social network API.