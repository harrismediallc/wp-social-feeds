<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @since             0.1.0
 * @package           WP_Social_Feeds
 *
 * @wordpress-plugin
 * Plugin Name:       WP Social Feeds
 * Description:       A plugin for retrieving and caching social feed data.
 * Version:           0.1.0
 * Author:            Harris Media, LLC
 * Author URI:        http://www.harrismediallc.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wp-social-feeds
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The core plugin class
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wp-social-feeds.php';

/**
 * Begins execution of the plugin.
 *
 * @since    0.1.0
 */
function run_wp_social_feeds() {

	$wp_social_feeds = new WP_Social_Feeds();
	$wp_social_feeds->run();

}
run_wp_social_feeds();
