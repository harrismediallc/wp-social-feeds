<?php

/**
 * The class responsible for retrieving Facebook page feeds
 *
 * @since      0.1.0
 *
 * @package    WP_Social_Feeds
 * @subpackage WP_Social_Feeds/admin/facebook
 */

/**
 * The class responsible for retrieving Facebook page feeds
 *
 * @package    WP_Social_Feeds
 * @subpackage WP_Social_Feeds/admin/facebook
 * @author     Daniel Abernathy <dabernathy89@gmail.com>
 */
class WP_Social_Feeds_Facebook {

    /**
     * Register the API route for this feed
     *
     * @since    0.1.0
     * @param   array    $routes    The registered REST API routes
     * @return  array               The registered REST API routes
     */
    public function rest_api_routes($routes) {

        $routes['facebook'] = array($this,'retrieve_facebook_feed');
        return $routes;

    }

    /**
     * Retrieve the Facebook feed
     *
     * @since    0.1.0
     * @param   WP_REST_Request    $request    The REST API request
     * @return  array|WP_Error                 The Facebook API response or WP_Error
     */
    public function retrieve_facebook_feed(WP_REST_Request $request) {

        $fb_options = get_option( 'wpsf_fb_settings', false );

        if (!$fb_options['wpsf_fb_app_id'] || !$fb_options['wpsf_fb_app_secret']) {
            return new WP_Error(
                'fb_app_settings_empty',
                'Facebook App Settings Empty',
                array( 'status' => 500 )
            );
        }

        if (!$request['fbpageid']) {
            return new WP_Error(
                'fb_no_page_id',
                'Facebook Page ID Not Specified',
                array( 'status' => 400 )
            );
        }

        // Define defaults for the post fields to grab
        $fields = ($request['fbfields']) ? $request['fbfields'] : "from,object_id,message,story,created_time,id,full_picture,picture,status_type,type,link,name,actions";

        // Build request URL
        $url = "https://graph.facebook.com/v2.4/";
        $url .= $request['fbpageid'] . "/posts";
        $url .= "?access_token=" . $fb_options['wpsf_fb_app_id'] . "|" . $fb_options['wpsf_fb_app_secret'];
        $url .= "&fields=" . $fields;

        return $this->make_request($url);

    }

    /**
     * Get the feed from the cache, or make the request for the feed to FB
     *
     * @since    0.1.0
     * @param   string    $url                 The URL of the request to Facebook
     * @return  array|WP_Error                 The Facebook API response or WP_Error
     */
    public function make_request($url) {

        $cache_key = 'wpsf_' . md5($url);

        $feed = get_transient($cache_key);

        if ($feed === false) {
            $response = wp_safe_remote_get($url);

            if (!is_wp_error($response)) {
                $feed = json_decode($response['body'],true);
                set_transient( $cache_key, $feed, HOUR_IN_SECONDS );
            } else {
                return $response;
            }
        }

        return $feed;

    }

}
