<?php

/**
 * Plugin settings page
 *
 * @since      0.1.0
 *
 * @package    WP_Social_Feeds
 * @subpackage WP_Social_Feeds/admin/settings
 */

/**
 * Plugin settings page
 *
 * @package    WP_Social_Feeds
 * @subpackage WP_Social_Feeds/admin/settings
 * @author     Daniel Abernathy <dabernathy89@gmail.com>
 */
class WP_Social_Feeds_Settings {

    /**
     * The ID of this plugin.
     *
     * @since    0.1.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * Initialize the class and set its properties.
     *
     * @since    0.1.0
     * @param      string    $plugin_name       The name of this plugin.
     */
    public function __construct( $plugin_name ) {

        $this->plugin_name = $plugin_name;

    }

    /**
     * Add the menu item
     *
     * @since    0.1.0
     */
    public function add_admin_menu() {

        add_options_page(
            'WP Social Feeds',
            'WP Social Feeds',
            'manage_options',
            $this->plugin_name . '_settings',
            array($this,'options_page')
        );

    }

    /**
     * Initialize the settings
     *
     * @since    0.1.0
     */
    public function settings_init() {

        register_setting( 'WPSF_FB_Settings_Group', 'wpsf_general_settings' );

        add_settings_section(
            'wpsf_general_settings_section',
            __( 'General Settings', 'wp-social-feeds' ),
            false,
            $this->plugin_name . '_settings'
        );

        add_settings_field(
            'wpsf_disable_cors_headers',
            __( 'Remove REST API CORS Headers', 'wp-social-feeds' ),
            array($this,'disable_cors_headers_render'),
            $this->plugin_name . '_settings',
            'wpsf_general_settings_section'
        );

        register_setting( 'WPSF_FB_Settings_Group', 'wpsf_fb_settings' );

        add_settings_section(
            'wpsf_fb_settings_section',
            __( 'Facebook App Settings', 'wp-social-feeds' ),
            false,
            $this->plugin_name . '_settings'
        );

        add_settings_field(
            'wpsf_fb_app_id',
            __( 'App ID', 'wp-social-feeds' ),
            array($this,'fb_app_id_render'),
            $this->plugin_name . '_settings',
            'wpsf_fb_settings_section'
        );

        add_settings_field(
            'wpsf_fb_app_secret',
            __( 'App Secret', 'wp-social-feeds' ),
            array($this,'fb_app_secret_render'),
            $this->plugin_name . '_settings',
            'wpsf_fb_settings_section'
        );

    }

    /**
     * Render the Facebook App ID setting
     *
     * @since    0.1.0
     */
    public function fb_app_id_render() {

        $options = get_option( 'wpsf_fb_settings' );
        echo "<input type='text' name='wpsf_fb_settings[wpsf_fb_app_id]' value='" . $options['wpsf_fb_app_id'] . "'>";

    }

    /**
     * Render the Facebook App Secret setting
     *
     * @since    0.1.0
     */
    public function fb_app_secret_render() {

        $options = get_option( 'wpsf_fb_settings' );
        echo "<input type='text' name='wpsf_fb_settings[wpsf_fb_app_secret]' value='" . $options['wpsf_fb_app_secret'] . "'>";

    }

    /**
     * Render the Disable REST API CORS Headers Output setting
     *
     * @since    0.1.0
     */
    public function disable_cors_headers_render() {

        $options = get_option( 'wpsf_general_settings' );

        if (!$options || !is_array($options)) {
            $options = array(
                'wpsf_disable_cors_headers' => false
            );
        }

        echo "<input type='checkbox' name='wpsf_general_settings[wpsf_disable_cors_headers]' ";
        checked( $options['wpsf_disable_cors_headers'], 1 );
        echo " value='1'>";

    }

    /**
     * Render the settings page
     *
     * @since    0.1.0
     */
    public function options_page() {

        echo "<form action='options.php' method='post'>";

        echo "<h2>WP Social Feeds Settings</h2>";

        echo "<hr>";

        settings_fields( 'WPSF_FB_Settings_Group' );
        do_settings_sections( $this->plugin_name . '_settings' );
        submit_button();

        echo "</form>";

    }
}
