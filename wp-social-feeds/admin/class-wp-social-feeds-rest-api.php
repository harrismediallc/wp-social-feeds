<?php

/**
 * Register REST API endpoints and handle requests
 *
 * @since      0.1.0
 *
 * @package    WP_Social_Feeds
 * @subpackage WP_Social_Feeds/admin/rest-api
 */

/**
 * Register REST API endpoints and handle requests
 *
 * @package    WP_Social_Feeds
 * @subpackage WP_Social_Feeds/admin/rest-api
 * @author     Daniel Abernathy <dabernathy89@gmail.com>
 */
class WP_Social_Feeds_REST_API {

    /**
     * The ID of this plugin.
     *
     * @since    0.1.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * Initialize the class and set its properties.
     *
     * @since    0.1.0
     * @param      string    $plugin_name       The name of this plugin.
     */
    public function __construct( $plugin_name ) {

        $this->plugin_name = $plugin_name;

    }

    /**
     * Register the REST API routes
     *
     * @since    0.1.0
     */
    public function rest_api_init() {

        $routes = apply_filters( $this->plugin_name . '_rest_api_routes', array() );

        foreach ($routes as $route => $callback) {
            register_rest_route(
                $this->plugin_name . '/v1',
                $route,
                array(
                    'methods' => 'GET',
                    'callback' => $callback
                )
            );
        }
    }

    /**
     * Optionally remove CORS headers
     *
     * @since    0.1.0
     */
    public function remove_cors_headers($value) {

        $options = get_option( 'wpsf_general_settings' );

        if (isset($options['wpsf_disable_cors_headers']) && $options['wpsf_disable_cors_headers'] === "1") {
            header_remove('Access-Control-Allow-Origin');
            header_remove('Access-Control-Allow-Methods');
            header_remove('Access-Control-Allow-Credentials');
        }

        return $value;
    }

}