<?php

/**
 * The file that defines the core plugin class
 *
 * @since      0.1.0
 *
 * @package    WP_Social_Feeds
 * @subpackage WP_Social_Feeds/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      0.1.0
 * @package    WP_Social_Feeds
 * @subpackage WP_Social_Feeds/includes
 * @author     Daniel Abernathy <dabernathy89@gmail.com>
 */
class WP_Social_Feeds {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    0.1.0
	 * @access   protected
	 * @var      WP_Social_Feeds_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    0.1.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    0.1.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    0.1.0
	 */
	public function __construct() {

		$this->plugin_name = 'wp-social-feeds';
		$this->version = '0.1.0';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    0.1.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wp-social-feeds-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wp-social-feeds-i18n.php';

		/**
		 * Plugin settings page
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-wp-social-feeds-settings.php';

		/**
		 * REST API integration
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-wp-social-feeds-rest-api.php';

		/**
		 * The class responsible for retrieving Facebook page feeds
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-wp-social-feeds-facebook.php';

		$this->loader = new WP_Social_Feeds_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the WP_Social_Feeds_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    0.1.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new WP_Social_Feeds_i18n();
		$plugin_i18n->set_domain( $this->get_plugin_name() );

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    0.1.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		// Plugin settings
		$plugin_settings = new WP_Social_Feeds_Settings( $this->get_plugin_name() );

		$this->loader->add_action( 'admin_menu', $plugin_settings, 'add_admin_menu' );
		$this->loader->add_action( 'admin_init', $plugin_settings, 'settings_init' );

		// REST API
		$rest_api = new WP_Social_Feeds_REST_API( $this->get_plugin_name() );

		$this->loader->add_action( 'rest_api_init', $rest_api, 'rest_api_init' );
		$this->loader->add_filter( 'rest_pre_serve_request', $rest_api, 'remove_cors_headers', 20 );

		// Facebook Feeds
		$facebook = new WP_Social_Feeds_Facebook();

		$this->loader->add_filter( $this->get_plugin_name() . '_rest_api_routes', $facebook, 'rest_api_routes' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    0.1.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     0.1.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     0.1.0
	 * @return    WP_Social_Feeds_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     0.1.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
