=== WP Social Feeds ===
Contributors: dabernathy89
Requires at least: 4.0
Tested up to: 4.3
Stable tag: 0.1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A plugin for retrieving and caching social feed data.

== Description ==

A plugin for retrieving and caching social feed data. This plugin simply acts as a 'proxy' between social network APIs and your theme's JavaScript code. It does not store individual feed items as WordPress posts; rather, it simply caches the feeds and delivers them to your JavaScript as if they were being sent directly from the social network API.

== Installation ==

1. Upload the plugin folder to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 0.1.0 =
* Initial version