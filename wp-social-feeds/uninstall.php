<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * @since      0.1.0
 *
 * @package    WP_Social_Feeds
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}
